## Mongo DB - Sentencias

NoSQL : SQL

Base de datos : bases de datos
Colecciones : tablas
Documentos : registros de una tabla
Schema : estructura que se debe seguir para insertar un registro.

> Los documentos se guardan en formato BSON, este es la codificación binaria de un documento JSON.

Extra information:
[OPERATORS IN Mongo DB](https://docs.mongodb.com/manual/reference/operator/)

### Comandos

>El simbolo > es usado como referencia de la consola.

`> dbs` = nos muestra que bases de datos tenemos.

`> use nombre-db` = para crear una base de datos.

>Por defecto cuando creas una base de datos, mongo te crea por default una colección.

Para revisar la ayuda de que puedes hacer sobre una colección:

`> db.coleccion.help`

> En donde dice colección se le pone el nombre de la colección.

Para buscar el primer registro de tu colección:

`> db.coleccion.findOne()`

Si tú agregas un registro y no le específicas el ID , mongo te lo crea por defecto. Tu le puedes setear el ID de está manera:

`> db.coleccion.insertToOne({ _id: "tu ID" .... })`

Ejemplo de agregar dato a una propiedad:

`> db.coleccion.insertToOne({ datos: algo })`

>Recuerda que no puedes guardar ids repetidos por eso mongo te genera uno en automático.

Para insertar varios documentos:

`> db.coleccion.insertMany(json)`

> Mongo db es atomico, esto quiere decir que si un documento de muchos no sé pudo guardar, esto hace un rollback para que no siga insertando y además borra los que ya inserto previamente.

Para enlistar las colecciones que tenemos:

`> show collections`

Para buscar algo en específico:

`> db.coleccion.findOne({ dato: "algo" })`

Lo mismo pero con datos específicos, como un select: SELECT name, age FROM table WHERE dato = "algo"

`> db.coleccion.findOne({ dato: "algo" }, { name: 1, age: 1} )`

Si te lo devuelve en algo que no se pueda entender le pones este comando .pretty :

`> db.coleccion.findOne({ dato: "algo" }).pretty()`

Para saber cuántos documentos son:

`> db.coleccion.findOne({ dato: "algo" }).count()`

Para hacer el where de in igual:

`> db.coleccion.findOne({ _id: ObjectId('ID')  })`

Para actualizar buen registro:

`> db.coleccion.updateOne({ filtro, {$set: {campo: "algo"} }}`

Para muchos sería con updateMany

Para borrar un registro:

`> db.coleccion.deleteOne({ filtro })`

Para borrar varios documentos con el mismo filtro:

`> db.coleccion.deleteMany({ filtro })`